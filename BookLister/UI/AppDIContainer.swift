//
//  AppDIContainer.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation
import UIKit

class AppDIContainer {
    private let baseURL = "https://my-json-server.typicode.com/KeskoSenukaiDigital/assignment"
    
    private lazy var booksService = DefaultBooksService(
        networkService: DefaultNetworkService(
            urlString: baseURL
        )
    )
    private lazy var bookListsStorage = DefaultBookListsStorage()
    private lazy var booksStorage = DefaultBooksStorage()
    
    func makeAppCoordinator(window: UIWindow) -> AppCoordinator {
        AppCoordinator(window: window, dependencies: self)
    }
}

extension AppDIContainer: AppCoordinatorDependencies {
    func makeBooksCoordinator(rootController: UINavigationController) -> BooksCoordinator {
        BooksDIContainer(
            dependencies: BooksDIContainer.Dependencies(
                booksService: booksService,
                bookListsStorage: bookListsStorage,
                booksStorage: booksStorage
            )
        )
        .makeBooksCoordinator(rootContorller: rootController)
    }
}
