//
//  AppCoordinator.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation
import UIKit

protocol AppCoordinatorDependencies {
    func makeBooksCoordinator(rootController: UINavigationController) -> BooksCoordinator
}

final class AppCoordinator: BaseCoordinator {
    private let window: UIWindow
    private let dependencies: AppCoordinatorDependencies
    private let rootController = UINavigationController()
    
    init(window: UIWindow, dependencies: AppCoordinatorDependencies) {
        self.window = window
        self.dependencies = dependencies
        
        window.rootViewController = rootController
        window.makeKeyAndVisible()
    }
    
    override func start() {
        let booksCoordinator = dependencies.makeBooksCoordinator(rootController: rootController)
        booksCoordinator.start()
        
        addChild(booksCoordinator)
    }
}
