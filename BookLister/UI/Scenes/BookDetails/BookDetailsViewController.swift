//
//  BookDetailsViewController.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//  Copyright (c) 2022 All rights reserved.
//

import UIKit

class BookDetailsViewController: BaseViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: BookDetailsViewModel!
    
    override func setupUI() {
        super.setupUI()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        tableView.tableFooterView = UIView()
        
        navigationItem.title = viewModel.screenTitle
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        viewModel.view = self
        
        viewModel.onReload = { [weak self] in
            self?.tableView.refreshControl?.endRefreshing()
            self?.tableView.reloadData()
        }
    }
}

// MARK: - User actions

private extension BookDetailsViewController {
    @objc private func didPullToRefresh() {
        viewModel.didPullToRefresh()
    }
}

// MARK: - UITableViewDataSource

extension BookDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let modelType = viewModel.cellType(at: indexPath) else { return UITableViewCell() }
        
        switch modelType {
        case .details(let model):
            let cell = tableView.dequeue(cell: BookDetailsCell.self)
            cell.setup(with: model)
            return cell
        case .imageCover(let url):
            let cell = tableView.dequeue(cell: BookCoverCell.self)
            cell.imageURL = url
            return cell
        }
    }
}
