//
//  BookDetailsCell.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit

struct BookDetailsCellModel {
    let title: String
    let author: String
    let isbn: String?
    let description: String
}

class BookDetailsCell: UITableViewCell, HasReusableIdentifier {
    @IBOutlet private weak var stackView: UIStackView!

    override func prepareForReuse() {
        super.prepareForReuse()
        
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
    }
    
    func setup(with model: BookDetailsCellModel) {
        stackView.spacing = 2
        
        setupTitleLabel(text: model.title)
        setpDetailsLabel(text: model.author)
        if let isbn = model.isbn {
            setpDetailsLabel(text: isbn)
        }
        if let lastView = stackView.arrangedSubviews.last {
            stackView.setCustomSpacing(8, after: lastView)
        }
        setpDetailsLabel(text: model.description)
    }
}

// MARK: - Private

private extension BookDetailsCell {
    func setupTitleLabel(text: String) {
        let label = UILabel()
        label.text = text
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = .preferredFont(forTextStyle: .title1, compatibleWith: nil)
        stackView.addArrangedSubview(label)
    }
    
    func setpDetailsLabel(text: String) {
        let label = UILabel()
        label.text = text
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = .preferredFont(forTextStyle: .subheadline, compatibleWith: nil)
        stackView.addArrangedSubview(label)
    }
}
