//
//  BookDetailsViewModel.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//  Copyright (c) 2022 All rights reserved.
//

import Foundation

protocol BookDetailsViewModelInput {
    func didPullToRefresh()
}

protocol BookDetailsViewModelOutput: HasViewModelView {
    var screenTitle: String { get }
    
    var numberOfRows: Int { get }
    func cellType(at indexPath: IndexPath) -> BookDetailsCellType?
    
    var onReload: (() -> Void)? { get set }
}

struct BookDetailsViewModelActions {
    
}

struct BookDetailsViewModelUseCases {
    let getBookDetails: GetBookDetailsUseCase
}

protocol BookDetailsViewModel: BookDetailsViewModelInput, BookDetailsViewModelOutput { }

class DefaultBookDetailsViewModel: BookDetailsViewModel {
    private let actions: BookDetailsViewModelActions
    private let useCases: BookDetailsViewModelUseCases
    
    private var bookDetails: BookDetails
    private var dataSource = [BookDetailsCellType]()
    
    let numberOfRows = 2
    let screenTitle: String
    
    var onReload: (() -> Void)?
    
    weak var view: ViewModelView?
    
    init(bookDetails: BookDetails, useCases: BookDetailsViewModelUseCases, actions: BookDetailsViewModelActions) {
        self.useCases = useCases
        self.actions = actions
        self.bookDetails = bookDetails
        self.screenTitle = bookDetails.title
        
        setupDataSource()
    }
    
    // MARK: - OUTPUT

    func cellType(at indexPath: IndexPath) -> BookDetailsCellType? {
        guard dataSource.indices.contains(indexPath.row) else { return nil }
        
        return dataSource[indexPath.row]
    }
}

// MARK: - INPUT. View event methods

extension DefaultBookDetailsViewModel {
    func didPullToRefresh() {
        useCases.getBookDetails.execute(bookID: bookDetails.id) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.view?.showError(error.bookServiceError)
            case .success(let details):
                self?.bookDetails = details
                self?.setupDataSource()
            }
        }
    }
}

// MARK: - Private

private extension DefaultBookDetailsViewModel {
    func setupDataSource() {
        dataSource = [
            .imageCover(URL(string: bookDetails.imageURL)),
            .details(
                BookDetailsCellModel(
                    title: bookDetails.title,
                    author: bookDetails.author,
                    isbn: bookDetails.isbn,
                    description: bookDetails.description
                )
            )
        ]
        onReload?()
    }
}
