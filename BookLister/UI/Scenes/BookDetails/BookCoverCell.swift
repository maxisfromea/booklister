//
//  BookCoverCell.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit
import SDWebImage

class BookCoverCell: UITableViewCell, HasReusableIdentifier {
    @IBOutlet private weak var imageViewCover: UIImageView!
    
    var imageURL: URL? {
        didSet {
            imageViewCover.sd_setImage(with: imageURL)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageViewCover.image = nil
    }
    
}
