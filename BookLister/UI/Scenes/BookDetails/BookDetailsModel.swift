//
//  BookDetailsModel.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

enum BookDetailsCellType {
    case imageCover(URL?)
    case details(BookDetailsCellModel)
}
