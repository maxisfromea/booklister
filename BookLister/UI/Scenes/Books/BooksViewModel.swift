//
//  BooksViewModel.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//  Copyright (c) 2022 All rights reserved.
//

import Foundation

protocol BooksViewModelInput {
    func didSelectItem(at indexPath: IndexPath)
    func didPulltoRefresh()
    func viewDidload()
}

protocol BooksViewModelOutput: HasViewModelView {
    var screenTitle: String { get }
    
    var numberOfRows: Int { get }
    func cellModel(at indexPath: IndexPath) -> BookCellModel?
    
    var onReload: (() -> Void)? { get set }
    var onIndexPathsReload: (([IndexPath]) -> Void)? { get set }
}

struct BooksViewModelActions {
    let onShowBookDetails: (BookDetails) -> Void
}

struct BooksViewModelUseCases {
    let getBooks: GetBooksUseCase
    let getBookDetails: GetBookDetailsUseCase
}

protocol BooksViewModel: BooksViewModelInput, BooksViewModelOutput { }

class DefaultBooksViewModel: BooksViewModel {
    private let detailsQueue = DispatchQueue(label: "details.booklister.queue", qos: .userInteractive)
    private let dataSourceQueue = DispatchQueue(label: "datasource.booklister.queue", qos: .userInteractive)
    private let networkQueue = DispatchQueue(label: "network.booklister.queue", qos: .userInteractive, attributes: .concurrent)
    
    private let actions: BooksViewModelActions
    private let useCases: BooksViewModelUseCases
    
    private var books: [Book]
    private var dataSource = [BookCellModel]()
    private var details = [Int: BookDetails]()
    
    let screenTitle: String
    
    var numberOfRows: Int { dataSource.count }
    
    var onReload: (() -> Void)?
    var onIndexPathsReload: (([IndexPath]) -> Void)?
    
    weak var view: ViewModelView?
    
    init(books: [Book], screenTitle: String, useCases: BooksViewModelUseCases, actions: BooksViewModelActions) {
        self.useCases = useCases
        self.actions = actions
        
        self.books = books.sorted(by: { $0.title < $1.title })
        self.screenTitle = screenTitle
        
        dataSource = self.books.map { BookCellModel(id: $0.id, title: $0.title, author: "Loading...", imageURL: URL(string: $0.imageURL)) }
    }
    
    // MARK: - OUTPUT

    func cellModel(at indexPath: IndexPath) -> BookCellModel? {
        guard dataSource.indices.contains(indexPath.row) else { return nil }
        
        return dataSource[indexPath.row]
    }
}

// MARK: - INPUT. View event methods

extension DefaultBooksViewModel {
    func didSelectItem(at indexPath: IndexPath) {
        let bookID = dataSource[indexPath.row].id
        guard let bookDetails = details[bookID] else { return }
        
        actions.onShowBookDetails(bookDetails)
    }
    
    func didPulltoRefresh() {
        loadBooks()
    }
    
    func viewDidload() {
        loadDetails()
    }
}

// MARK: - Private

private extension DefaultBooksViewModel {
    func loadBooks() {
        books.removeAll()
        details.removeAll()
        
        useCases.getBooks.execute(
            cached: { books in
                
            },
            completion: { [weak self] result in
                switch result {
                case .failure(let error):
                    self?.view?.showError(error.bookServiceError)
                case .success(let books):
                    self?.books = books
                    self?.loadDetails()
                }
            }
        )
    }
    
    func loadDetails() {
        books.forEach { book in
            let bookID = book.id
            
            let item = DispatchWorkItem { [weak self] in
                self?.useCases.getBookDetails.execute(bookID: bookID) { result in
                    switch result {
                    case .failure(let error):
                        #if DEBUG
                        print(error)
                        #endif
                        self?.updateDataSourceAuhtor(with: "N/A", bookID: bookID)
                    case .success(let details):
                        self?.updateDataSourceAuhtor(with: details.author, bookID: bookID)
                        self?.setBookDetails(details, for: bookID)
                    }
                }
            }
            networkQueue.async(execute: item)
        }
    }
    
    func setBookDetails(_ details: BookDetails, for bookID: Int) {
        detailsQueue.async { [weak self] in self?.details[bookID] = details }
    }
    
    func updateDataSourceAuhtor(with author: String, bookID: Int) {
        dataSourceQueue.async { [weak self] in
            guard
                let index = self?.dataSource.firstIndex(where: { $0.id == bookID })
            else {
                return
            }
            
            self?.dataSource[index].author = author
            
            DispatchQueue.main.async {
                self?.onIndexPathsReload?([IndexPath(row: index, section: 0)])
            }
        }
    }
}
