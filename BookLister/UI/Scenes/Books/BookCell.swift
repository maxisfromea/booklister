//
//  BookCell.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit
import SDWebImage

struct BookCellModel {
    let id: Int
    let title: String
    var author: String
    let imageURL: URL?
}

class BookCell: UITableViewCell, HasReusableIdentifier {
    @IBOutlet private weak var labelBookTitle: UILabel!
    @IBOutlet private weak var labelBookAuthor: UILabel!
    @IBOutlet private weak var imageViewCover: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageViewCover.image = nil
    }
    
    func setup(with model: BookCellModel) {
        labelBookTitle.text = model.title
        labelBookAuthor.text = model.author
        imageViewCover.sd_setImage(with: model.imageURL)
    }
}
