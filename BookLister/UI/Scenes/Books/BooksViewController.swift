//
//  BooksViewController.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//  Copyright (c) 2022 All rights reserved.
//

import UIKit

class BooksViewController: BaseViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    var viewModel: BooksViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidload()
    }
    
    override func setupUI() {
        super.setupUI()
        
        navigationItem.title = viewModel.screenTitle
        
        tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        viewModel.view = self
        
        viewModel.onIndexPathsReload = { [weak self] in
            if self?.tableView.refreshControl?.isRefreshing == true {
                self?.tableView.refreshControl?.endRefreshing()
            }
            self?.tableView.reloadRows(at: $0, with: .none)
        }
    }
}

// MARK: - User actions

private extension BooksViewController {
    @objc private func didPullToRefresh() {
        viewModel.didPulltoRefresh()
    }
}

// MARK: - UITableViewDataSource

extension BooksViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellModel = viewModel.cellModel(at: indexPath) else { return UITableViewCell() }
        
        let cell = tableView.dequeue(cell: BookCell.self)
        cell.setup(with: cellModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension BooksViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectItem(at: indexPath)
    }
}
