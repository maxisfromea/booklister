//
//  BooksDIContainer.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation
import UIKit

struct BooksDIContainer {
    struct Dependencies {
        let booksService: BooksService
        let bookListsStorage: BookListsStorage
        let booksStorage: BooksStorage
    }
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func makeBooksCoordinator(rootContorller: UINavigationController) -> BooksCoordinator {
        BooksCoordinator(dependencies: self, rootController: rootContorller)
    }
}

// MARK: - BooksCoordinatorDependencies

extension BooksDIContainer: BooksCoordinatorDependencies {
    func makeBookListsScene(actions: BookListsViewModelActions) -> UIViewController {
        let viewController = BookListsViewController.instantiateViewController(nil)
        viewController.viewModel = DefaultBookListsViewModel(
            useCases: BookListsViewModelUseCases(
                getBookLists: DefaultGetBookListsUseCase(
                    repository: DefaultGetBookListsRepository(
                        service: dependencies.booksService,
                        storage: dependencies.bookListsStorage
                    )
                ),
                getBooks: DefaultGetBooksUseCase(
                    repository: DefaultGetBooksRepository(
                        service: dependencies.booksService,
                        storage: dependencies.booksStorage
                    )
                ),
                getBookDetails: DefaultGetBookDetailsUseCase(
                    repository: DefaultGetBookDetailsRepository(
                        service: dependencies.booksService
                    )
                )
            ),
            actions: actions
        )
        return viewController
    }
    
    func makeBooksScene(books: [Book], screenTitle: String, actions: BooksViewModelActions) -> UIViewController {
        let viewController = BooksViewController.instantiateViewController(nil)
        viewController.viewModel = DefaultBooksViewModel(
            books: books,
            screenTitle: screenTitle,
            useCases: BooksViewModelUseCases(
                getBooks: DefaultGetBooksUseCase(
                    repository: DefaultGetBooksRepository(
                        service: dependencies.booksService,
                        storage: dependencies.booksStorage
                    )
                ),
                getBookDetails: DefaultGetBookDetailsUseCase(
                    repository: DefaultGetBookDetailsRepository(
                        service: dependencies.booksService
                    )
                )
            ),
            actions: actions
        )
        return viewController
    }
    
    func makeBookDetailsScene(bookDetails: BookDetails, actions: BookDetailsViewModelActions) -> UIViewController {
        let viewController = BookDetailsViewController.instantiateViewController(nil)
        viewController.viewModel = DefaultBookDetailsViewModel(
            bookDetails: bookDetails,
            useCases: BookDetailsViewModelUseCases(
                getBookDetails: DefaultGetBookDetailsUseCase(
                    repository: DefaultGetBookDetailsRepository(
                        service: dependencies.booksService
                    )
                )
            ),
            actions: actions
        )
        return viewController
    }
}
