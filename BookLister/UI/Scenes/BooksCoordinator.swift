//
//  BooksCoordinator.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation
import UIKit

protocol BooksCoordinatorDependencies {
    func makeBookListsScene(actions: BookListsViewModelActions) -> UIViewController
    func makeBooksScene(books: [Book], screenTitle: String, actions: BooksViewModelActions) -> UIViewController
    func makeBookDetailsScene(bookDetails: BookDetails, actions: BookDetailsViewModelActions) -> UIViewController
}

final class BooksCoordinator: BaseCoordinator {
    private let dependencies: BooksCoordinatorDependencies
    private let rootController: UINavigationController
    
    init(dependencies: BooksCoordinatorDependencies, rootController: UINavigationController) {
        self.dependencies = dependencies
        self.rootController = rootController
    }
    
    override func start() {
        let viewController = dependencies.makeBookListsScene(
            actions: BookListsViewModelActions(
                onShowBooks: { [weak self] books, screnTitle in
                    self?.showBooks(books, screenTitle: screnTitle)
                },
                onShowBookDetails: { [weak self] details in
                    self?.showBookDetails(details)
                }
            )
        )
        rootController.show(viewController, sender: self)
    }
}

// MARK: - Navigation

private extension BooksCoordinator {
    func showBooks(_ books: [Book], screenTitle: String) {
        let viewController = dependencies.makeBooksScene(
            books: books,
            screenTitle: screenTitle,
            actions: BooksViewModelActions(
                onShowBookDetails: { [weak self] bookDetails in
                    self?.showBookDetails(bookDetails)
                }
            )
        )
        rootController.show(viewController, sender: self)
    }
    
    func showBookDetails(_ bookDetails: BookDetails) {
        let viewController = dependencies.makeBookDetailsScene(
            bookDetails: bookDetails,
            actions: BookDetailsViewModelActions()
        )
        rootController.show(viewController, sender: self)
    }
}
