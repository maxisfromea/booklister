//
//  BookListsViewController.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//  Copyright (c) 2022 All rights reserved.
//

import UIKit

class BookListsViewController: BaseViewController {
    @IBOutlet private weak var collectionView: UICollectionView!
    
    var viewModel: BookListsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
    }
    
    override func setupUI() {
        super.setupUI()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = CGSize(width: 90, height: 150)
            layout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 44)
        }
        
        collectionView.register(
            BookListsHeaderView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: BookListsHeaderView.reusableIdentifier
        )
        
        navigationItem.title = "BookLister"
        
        setupCollectionViewLayout()
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        viewModel.view = self
        
        viewModel.onReload = { [weak self] in
            self?.collectionView.reloadData()
        }
    }
}

// MARK: - User actions

private extension BookListsViewController {
    @objc private func didPullToRefresh() {
        viewModel.didPullToRefresh()
        
        collectionView.refreshControl?.endRefreshing()
    }
}

// MARK: - Private

private extension BookListsViewController {
    func setupCollectionViewLayout() {
        collectionView.setCollectionViewLayout(
            UICollectionViewCompositionalLayout(
                section: generateFeaturedAlbumsLayout()
            ),
            animated: false
        )
    }
    
    func generateFeaturedAlbumsLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(4/3)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupFractionalWidth = 0.4
        let groupFractionalHeight: Float = 5/9
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(CGFloat(groupFractionalWidth)),
            heightDimension: .fractionalWidth(CGFloat(groupFractionalHeight))
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitem: item,
            count: 1
        )
        
        let headerSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(44)
        )
        let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: headerSize,
            elementKind: UICollectionView.elementKindSectionHeader,
            alignment: .top
        )
        
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [sectionHeader]
        section.orthogonalScrollingBehavior = .continuous
        
        return section
    }
}

// MARK: - UICollectionViewDataSource

extension BookListsViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        viewModel.numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfRows(in: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cellModel = viewModel.cellModel(at: indexPath) else { return UICollectionViewCell() }
        
        let cell = collectionView.dequeue(cell: BookListCell.self, for: indexPath)
        cell.setup(with: cellModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard
            kind == UICollectionView.elementKindSectionHeader,
            let header = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: BookListsHeaderView.reusableIdentifier,
                for: indexPath
            ) as? BookListsHeaderView
        else {
            return UICollectionReusableView()
        }

        header.title = viewModel.titleForHeader(at: indexPath.section)
        header.onTap = { [weak self] in self?.viewModel.didTapAll(at: indexPath.section) }
        return header
    }
}

// MARK: - UICollectionViewDelegate

extension BookListsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelectItem(at: indexPath)
    }
}
