//
//  BookListsViewModel.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//  Copyright (c) 2022 All rights reserved.
//

import Foundation

protocol BookListsViewModelInput {
    func didSelectItem(at indexPath: IndexPath)
    func didTapAll(at section: Int)
    func didPullToRefresh()
    func viewDidLoad()
}

protocol BookListsViewModelOutput: HasViewModelView {
    var numberOfSections: Int { get }
    func numberOfRows(in section: Int) -> Int
    func cellModel(at indexPath: IndexPath) -> BookListCellModel?
    func titleForHeader(at section: Int) -> String?
    
    var onReload: (() -> Void)? { get set }
}

struct BookListsViewModelActions {
    let onShowBooks: ([Book], String) -> Void
    let onShowBookDetails: (BookDetails) -> Void
}

struct BookListsViewModelUseCases {
    let getBookLists: GetBookListsUseCase
    let getBooks: GetBooksUseCase
    let getBookDetails: GetBookDetailsUseCase
}

protocol BookListsViewModel: BookListsViewModelInput, BookListsViewModelOutput { }

class DefaultBookListsViewModel: BookListsViewModel {
    private let actions: BookListsViewModelActions
    private let useCases: BookListsViewModelUseCases
    
    private var dataSource = [Int: [BookListCellModel]]()
    private var books = [Book]()
    private var bookLists = [BookList]()
    
    var numberOfSections: Int { dataSource.keys.count }
    var onReload: (() -> Void)?
    
    weak var view: ViewModelView?
    
    init(useCases: BookListsViewModelUseCases, actions: BookListsViewModelActions) {
        self.useCases = useCases
        self.actions = actions
    }
    
    // MARK: - OUTPUT

    func numberOfRows(in section: Int) -> Int {
        min(dataSource[section]?.count ?? 0, 5)
    }
    
    func cellModel(at indexPath: IndexPath) -> BookListCellModel? {
        guard
            let rows = dataSource[indexPath.section],
            rows.indices.contains(indexPath.row)
        else {
            return nil
        }
        
        return rows[indexPath.row]
    }
    
    func titleForHeader(at section: Int) -> String? {
        guard bookLists.indices.contains(section) else { return nil }
        
        return bookLists[section].title
    }
}

// MARK: - INPUT. View event methods

extension DefaultBookListsViewModel {
    func didTapAll(at section: Int) {
        let title = bookLists[section].title
        let books = books.filter { $0.listID == bookLists[section].id }
        
        actions.onShowBooks(books, title)
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        guard let bookID = dataSource[indexPath.section]?[indexPath.row].id else { return }
        
        getBookDetails(for: bookID)
    }
    
    func didPullToRefresh() {
        loadBookLists()
    }
    
    func viewDidLoad() {
        loadBookLists()
    }
}

// MARK: - Private

private extension DefaultBookListsViewModel {
    func loadBookLists() {
        view?.setLoader(isLoading: true)
        
        useCases.getBookLists.execute(
            cached: { [weak self] bookLists in
                self?.bookLists = bookLists
                self?.loadBooks(bookLists: bookLists)
            },
            completion: { [weak self] result in
                switch result {
                case .failure(let error):
                    self?.view?.showError(error.bookServiceError)
                case .success(let bookLists):
                    self?.bookLists = bookLists
                    self?.loadBooks(bookLists: bookLists)
                }
            }
        )
    }
    
    func loadBooks(bookLists: [BookList]) {
        useCases.getBooks.execute(
            cached: { [weak self] books in
                self?.books = books
                self?.setupDataSource(with: bookLists, books: books)
            },
            completion: { [weak self] result in
                switch result {
                case .failure(let error):
                    self?.view?.showError(error.bookServiceError)
                case .success(let books):
                    self?.books = books
                }
                self?.setupDataSource(with: bookLists, books: self?.books ?? [])
            }
        )
    }
    
    func setupDataSource(with bookLists: [BookList], books: [Book]) {
        dataSource.removeAll()
        
        var counter = 0
        bookLists.forEach { bookList in
            self.dataSource[counter] = books
                .filter { $0.listID == bookList.id }
                .map { BookListCellModel(id: $0.id, title: $0.title, imageURL: URL(string: $0.imageURL)) }
            counter += 1
        }
        
        view?.setLoader(isLoading: false)
        onReload?()
    }
    
    func getBookDetails(for bookID: Int) {
        view?.setLoader(isLoading: true)
        
        useCases.getBookDetails.execute(bookID: bookID) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.view?.showError(error.bookServiceError)
            case .success(let details):
                self?.actions.onShowBookDetails(details)
            }
            self?.view?.setLoader(isLoading: false)
        }
    }
}
