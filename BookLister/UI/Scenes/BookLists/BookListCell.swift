//
//  BookListCell.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit
import SDWebImage

struct BookListCellModel {
    let id: Int
    let title: String
    let imageURL: URL?
}

class BookListCell: UICollectionViewCell, HasReusableIdentifier {
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var imageViewBook: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageViewBook.image = nil
    }
    
    func setup(with model: BookListCellModel) {
        labelTitle.text = model.title
        imageViewBook.sd_setImage(with: model.imageURL)
    }
}
