//
//  BookListsHeaderView.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit

class BookListsHeaderView: UICollectionReusableView, HasReusableIdentifier {
    private let labelTitle = UILabel()
    
    var onTap: (() -> Void)?
    
    var title: String? {
        didSet {
            labelTitle.text = title
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    @objc private func didTapSeeAll(_ sender: UIButton) {
        onTap?()
    }
    
    private func setup() {
        labelTitle.font = .preferredFont(forTextStyle: .title3)
        labelTitle.translatesAutoresizingMaskIntoConstraints = false
        addSubview(labelTitle)
        
        labelTitle.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        labelTitle.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        
        let button = UIButton(type: .system)
        button.setTitle("View all", for: .normal)
        button.addTarget(self, action: #selector(didTapSeeAll(_:)), for: .touchUpInside)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        
        button.topAnchor.constraint(equalTo: topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        button.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
}
