//
//  HasReusableIdentifier.swift
//  Berg
//
//  Created by Maximus on 14/03/2022.
//

import UIKit

protocol HasReusableIdentifier {
    static var reusableIdentifier: String { get }
}

extension HasReusableIdentifier where Self: UITableViewCell {
    static var reusableIdentifier: String { String(describing: Self.self) }
}

extension HasReusableIdentifier where Self: UICollectionViewCell {
    static var reusableIdentifier: String { String(describing: Self.self) }
}

extension HasReusableIdentifier where Self: UICollectionReusableView {
    static var reusableIdentifier: String { String(describing: Self.self) }
}
