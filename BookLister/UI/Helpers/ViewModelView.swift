//
//  ViewModelView.swift
//  BookLister
//
//  Created by Maximus on 13/03/2022.
//

import Foundation

protocol LoadingView: AnyObject {
    func setLoader(isLoading: Bool)
}

protocol ErrorView {
    func showError(_ error: String)
}

protocol ViewModelView: LoadingView, ErrorView { }

protocol HasViewModelView {
    var view: ViewModelView? { get set }
}
