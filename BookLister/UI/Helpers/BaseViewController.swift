//
//  BaseViewController.swift
//  Berg
//
//  Created by Maximus on 13/03/2022.
//

import UIKit

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    private lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .large)
        loader.color = .systemBlue
        loader.hidesWhenStopped = true
        loader.isHidden = true
        
        view.addSubview(loader)
        loader.center = view.center
        
        return loader
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupBindings()
    }
    
    func setupUI() {}
    func setupBindings() {}
    
    func showError(_ error: String) {
        setLoader(isLoading: false)
        
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .default))
        navigationController?.present(alert, animated: true)
    }
    
    func setLoader(isLoading: Bool) {
        if isLoading {
            loader.startAnimating()
            view.bringSubviewToFront(loader)
            loader.isHidden = false
        } else {
            loader.stopAnimating()
            view.sendSubviewToBack(loader)

        }
    }
}

extension BaseViewController: StoryboardInstantiable {}

extension BaseViewController: ViewModelView {}
