//
//  BaseCoordinator.swift
//  Berg
//
//  Created by Maximus on 13/03/2022.
//

import Foundation

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var onFinish: ((Coordinator?) -> ())? { get set }
    func start()
    func addChild(_ coordinator: Coordinator)
    func removeChild(_ coordinator: Coordinator?)
}

class BaseCoordinator: NSObject, Coordinator {
    var childCoordinators: [Coordinator] = []
    var onFinish: ((Coordinator?) -> ())?
    
    func start() {}
    
    func addChild(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }
    
    func removeChild(_ coordinator: Coordinator?) {
        guard let coordinator = coordinator else { return }
        
        if let index = childCoordinators.lastIndex(where: { $0 === coordinator }) {
            childCoordinators.remove(at: index)
        }
    }
}
