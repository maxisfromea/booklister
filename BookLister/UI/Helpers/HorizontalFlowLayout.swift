//
//  HorizontalFlowLayout.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit

protocol HorizontalFlowLayoutDelegate: AnyObject {
    func width(at index: Int) -> CGFloat?
}

final class HorizontalFlowLayout: UICollectionViewFlowLayout {
    private var lastElementMaxX: CGFloat = 0
    
    weak var delegate: HorizontalFlowLayoutDelegate?
    
    var padding: CGFloat = 30
    var cellHeight: CGFloat = 45
    var margin: CGFloat = 15
    var collectionViewHeight: CGFloat = 70
    
    var headerAttributes = [UICollectionViewLayoutAttributes]()
    var cachedAttributes = [UICollectionViewLayoutAttributes]()
    
    func setupHeaderAttributes() {
        for section in 0..<(collectionView?.numberOfSections ?? 0) {
            let frame = CGRect(
                x: 0,
                y: CGFloat(section) * cellHeight + CGFloat(section) * margin,
                width: UIScreen.main.bounds.width,
                height: 44
            )
            let sectionHeader = UICollectionViewLayoutAttributes(
                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                with: IndexPath(row: 0, section: section)
            )
            sectionHeader.frame = frame
            headerAttributes.append(sectionHeader)
        }
    }
    
    override func invalidateLayout() {
        headerAttributes.removeAll()
        cachedAttributes.removeAll()
        
        super.invalidateLayout()
    }
    
    override func prepare() {
        guard let collectionView = self.collectionView, cachedAttributes.isEmpty else { return }
        
        setupHeaderAttributes()
        lastElementMaxX = 0
        
        for section in 0..<collectionView.numberOfSections {
            lastElementMaxX = 0
            for index in 0..<collectionView.numberOfItems(inSection: section) {
                let indexPath = IndexPath(row: index, section: section)
                
                if let width = delegate?.width(at: index) {
                    let width = width + padding * 2
                    
                    let frame = CGRect(
                        x: lastElementMaxX,
                        y: CGFloat(section) * cellHeight + CGFloat(section) * margin,
                        width: CGFloat(width),
                        height: cellHeight)
                    
                    lastElementMaxX += (frame.width + margin)
                    
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                    attributes.frame = frame
                    
                    cachedAttributes.append(attributes)
                }
            }
        }
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath)
        -> UICollectionViewLayoutAttributes? {
      
        cachedAttributes[indexPath.item]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []

        for attributes in cachedAttributes {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        
        for attributes in headerAttributes {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        
        return visibleLayoutAttributes
    }
    
    override var collectionViewContentSize: CGSize {
        CGSize(
            width: lastElementMaxX - margin,
            height: collectionViewHeight
        )
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        headerAttributes[indexPath.section]
    }
}

