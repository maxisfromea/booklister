//
//  UITableViewExtensions.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit

extension UITableView {
    func dequeue<T: HasReusableIdentifier>(cell: T.Type) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: cell.reusableIdentifier) as? T else {
            fatalError("Could not dequeque cell \(cell.reusableIdentifier)")
        }
        return cell
    }
}
