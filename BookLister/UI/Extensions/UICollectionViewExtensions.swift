//
//  UICollectionViewExtensions.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import UIKit

extension UICollectionView {
    func dequeue<T: HasReusableIdentifier>(cell: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cell.reusableIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeque cell")
        }
        return cell
    }
}
