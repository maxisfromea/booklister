//
//  GetBookListsUseCase.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

protocol GetBookListsUseCase {
    func execute(cached: @escaping ([BookList]) -> Void, completion: @escaping (Result<[BookList], Error>) -> Void)
}

final class DefaultGetBookListsUseCase: GetBookListsUseCase {
    private let repository: GetBookListsRepository
    
    init(repository: GetBookListsRepository) {
        self.repository = repository
    }
    
    func execute(cached: @escaping ([BookList]) -> Void, completion: @escaping (Result<[BookList], Error>) -> Void) {
        repository.getBookLists(cached: cached, completion: completion)
    }
}
