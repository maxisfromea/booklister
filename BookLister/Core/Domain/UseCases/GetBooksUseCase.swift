//
//  GetBooksUseCase.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

protocol GetBooksUseCase {
    func execute(cached: @escaping ([Book]) -> Void, completion: @escaping (Result<[Book], Error>) -> Void)
}

final class DefaultGetBooksUseCase: GetBooksUseCase {
    private let repository: GetBooksRepository
    
    init(repository: GetBooksRepository) {
        self.repository = repository
    }
    
    func execute(cached: @escaping ([Book]) -> Void, completion: @escaping (Result<[Book], Error>) -> Void) {
        repository.getBooks(cached: cached, completion: completion)
    }
}
