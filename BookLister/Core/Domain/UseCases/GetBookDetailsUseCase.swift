//
//  GetBookDetailsUseCase.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

protocol GetBookDetailsUseCase {
    func execute(bookID: Int, completion: @escaping (Result<BookDetails, Error>) -> Void)
}

final class DefaultGetBookDetailsUseCase: GetBookDetailsUseCase {
    private let repository: GetBookDetailsRepository
    
    init(repository: GetBookDetailsRepository) {
        self.repository = repository
    }
    
    func execute(bookID: Int, completion: @escaping (Result<BookDetails, Error>) -> Void) {
        repository.getBookDetails(bookID: bookID, completion: completion)
    }
}
