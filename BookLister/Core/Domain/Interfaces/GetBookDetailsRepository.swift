//
//  GetBookDetailsRepository.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

protocol GetBookDetailsRepository {
    func getBookDetails(bookID: Int, completion: @escaping (Result<BookDetails, Error>) -> Void)
}
