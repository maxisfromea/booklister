//
//  GetBookListsRepository.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

protocol GetBookListsRepository {
    func getBookLists(cached: @escaping ([BookList]) -> Void, completion: @escaping (Result<[BookList], Error>) -> Void)
}
