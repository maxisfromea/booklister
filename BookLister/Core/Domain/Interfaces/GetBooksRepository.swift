//
//  GetBooksRepository.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

protocol GetBooksRepository {
    func getBooks(cached: @escaping ([Book]) -> Void, completion: @escaping (Result<[Book], Error>) -> Void)
}
