//
//  BookDetails.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

struct BookDetails {
    let id: Int
    let listID: Int
    let isbn: String?
    let author: String
    let title: String
    let imageURL: String
    let description: String
}
