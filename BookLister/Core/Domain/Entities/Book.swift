//
//  Book.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

struct Book {
    let id: Int
    let listID: Int
    let title: String
    let imageURL: String
}
