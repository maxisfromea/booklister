//
//  BookList.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

struct BookList {
    let id: Int
    let title: String
}
