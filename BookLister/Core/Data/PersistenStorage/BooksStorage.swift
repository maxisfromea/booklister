//
//  BooksStorage.swift
//  BookLister
//
//  Created by Maximus on 07/04/2022.
//

import Foundation
import CoreData

protocol BooksStorage {
    func fetchBooks(completion: @escaping (Result<[Book], Error>) -> Void)
    func saveBooks(_ books: [Book], completion: @escaping (Error?) -> Void)
}

final class DefaultBooksStorage: BooksStorage {
    private let storage: CoreDataStorage
    
    init(storage: CoreDataStorage = .shared) {
        self.storage = storage
    }
    
    func fetchBooks(completion: @escaping (Result<[Book], Error>) -> Void) {
        storage.perform { context in
            do {
                let request: NSFetchRequest = BookEntity.fetchRequest()
                request.sortDescriptors = [NSSortDescriptor(keyPath: \BookEntity.title, ascending: true)]
                let result = try context.fetch(request).map { $0.toDomain() }
                
                completion(.success(result))
            } catch {
                completion(.failure(CoreDataStorageError.readError(error)))
            }
        }
    }
    
    func saveBooks(_ books: [Book], completion: @escaping (Error?) -> Void) {
        storage.perform { [weak self] context in
            books.forEach {
                do {
                    try self?.removeDuplicate(for: $0, in: context)
                } catch {
                    completion(CoreDataStorageError.deleteError(error))
                }
                
                let bookEntity = BookEntity(context: context)
                bookEntity.title = $0.title
                bookEntity.bookID = Int64($0.id)
                bookEntity.imageURL = $0.imageURL
                bookEntity.listID = Int64($0.listID)
            }
            do {
                try context.save()
                completion(nil)
            } catch {
                completion(CoreDataStorageError.saveError(error))
            }
        }
    }
}

// MARK: - Private

private extension DefaultBooksStorage {
    func removeDuplicate(for book: Book, in context: NSManagedObjectContext) throws {
        let request: NSFetchRequest = BookEntity.fetchRequest()
        request.predicate = NSPredicate(format: "bookID = %d", book.id)
        request.fetchLimit = 1
        
        guard let result = try context.fetch(request).first else { return }
        
        context.delete(result)
    }
}
