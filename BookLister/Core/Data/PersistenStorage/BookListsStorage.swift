//
//  BookListsStorage.swift
//  BookLister
//
//  Created by Maximus on 06/04/2022.
//

import Foundation
import CoreData

protocol BookListsStorage {
    func fetchBookLists(completion: @escaping (Result<[BookList], Error>) -> Void)
    func saveBookLists(_ bookLists: [BookList], completion: @escaping (Error?) -> Void)
}

final class DefaultBookListsStorage: BookListsStorage {
    private let storage: CoreDataStorage
    
    init(storage: CoreDataStorage = .shared) {
        self.storage = storage
    }
    
    func fetchBookLists(completion: @escaping (Result<[BookList], Error>) -> Void) {
        storage.perform { context in
            do {
                let request = BookListEntity.fetchRequest()
                request.sortDescriptors = [NSSortDescriptor(keyPath: \BookListEntity.title, ascending: true)]
                let result = try context.fetch(request).map { $0.toDomain() }
                
                completion(.success(result))
            } catch {
                completion(.failure(CoreDataStorageError.readError(error)))
            }
        }
    }
    
    func saveBookLists(_ bookLists: [BookList], completion: @escaping (Error?) -> Void) {
        
        storage.perform { [weak self] context in
            bookLists.forEach {
                do {
                    try self?.removeDuplicate(for: $0, in: context)
                } catch {
                    completion(CoreDataStorageError.deleteError(error))
                }
                
                let bookListEntity = BookListEntity(context: context)
                bookListEntity.title = $0.title
                bookListEntity.bookListID = Int64($0.id)
            }
            do {
                try context.save()
                completion(nil)
            } catch {
                completion(CoreDataStorageError.saveError(error))
            }
        }
    }
}

// MARK: - Private

private extension DefaultBookListsStorage {
    func removeDuplicate(for bookList: BookList, in context: NSManagedObjectContext) throws {
        let request: NSFetchRequest = BookListEntity.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(BookListEntity.bookListID), ascending: true)]
        request.predicate = NSPredicate(format: "bookListID = %d", bookList.id)
        request.fetchLimit = 1
        
        guard let result = try context.fetch(request).first else { return }
        
        context.delete(result)
    }
}
