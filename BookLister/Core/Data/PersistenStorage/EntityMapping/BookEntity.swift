//
//  BookEntity.swift
//  BookLister
//
//  Created by Maximus on 07/04/2022.
//

import Foundation

extension BookEntity {
    func toDomain() -> Book {
        Book(id: Int(bookID), listID: Int(listID), title: title ?? "N/A", imageURL: imageURL ?? "")
    }
}
