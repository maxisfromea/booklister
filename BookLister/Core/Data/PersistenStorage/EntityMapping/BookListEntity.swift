//
//  BookListEntity.swift
//  BookLister
//
//  Created by Maximus on 06/04/2022.
//

import Foundation
import CoreData

extension BookListEntity {
    func toDomain() -> BookList {
        BookList(id: Int(bookListID), title: title ?? "N/A")
    }
}
