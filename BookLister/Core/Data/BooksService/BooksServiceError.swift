//
//  BooksServiceError.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

enum BooksServiceError: Error {
    case parsing
    case noData
    case custom(Error)
    
    var message: String {
        switch self {
        case .parsing: return "Unexpected response from server"
        case .noData: return "Did not receive data from server"
        case .custom(let error): return error.localizedDescription
        }
    }
}

extension Error {
    var bookServiceError: String {
        if let error = (self as? BooksServiceError) {
            return error.message
        } else {
            return localizedDescription
        }
    }
}
