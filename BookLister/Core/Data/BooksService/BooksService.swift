//
//  BooksService.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

protocol CanGetListsService {
    func getLists(completion: @escaping (Result<[BookListResponseDTO], BooksServiceError>) -> Void)
}

protocol CanGetBooksService {
    func getBooks(completion: @escaping (Result<[BookResponseDTO], BooksServiceError>) -> Void)
}

protocol CanGetBookDetailsService {
    func getBookDetails(bookID: Int, completion: @escaping (Result<BookDetailsResponseDTO, BooksServiceError>) -> Void)
}

protocol BooksService: CanGetListsService, CanGetBooksService, CanGetBookDetailsService {}

final class DefaultBooksService: BooksService {
    private let networkService: NetworkService
    
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    func getLists(completion: @escaping (Result<[BookListResponseDTO], BooksServiceError>) -> Void) {
        networkService.execute(BookListsRequest()) { [weak self] data, error in
            self?.handleResponse(data: data, error: error, completion: completion)
        }
    }
    
    func getBooks(completion: @escaping (Result<[BookResponseDTO], BooksServiceError>) -> Void) {
        networkService.execute(BooksRequest()) { [weak self] data, error in
            self?.handleResponse(data: data, error: error, completion: completion)
        }
    }
    
    func getBookDetails(bookID: Int, completion: @escaping (Result<BookDetailsResponseDTO, BooksServiceError>) -> Void) {
        networkService.execute(BookDetailsRequest(bookID: bookID)) { [weak self] data, error in
            self?.handleResponse(data: data, error: error, completion: completion)
        }
    }
}

// MARK: - Private

private extension DefaultBooksService {
    func handleResponse<T: Decodable>(data: Data?, error: Error?, completion: @escaping (Result<T, BooksServiceError>) -> Void) {
        if let error = error {
            completion(.failure(BooksServiceError.custom(error)))
        } else {
            parse(data, completion: completion)
        }
    }
    
    func parse<T: Decodable>(_ data: Data?, completion: @escaping (Result<T, BooksServiceError>) -> Void) {
        guard let data = data else {
            completion(.failure(.noData))
            return
        }
        
        do {
            let object = try JSONDecoder().decode(T.self, from: data)
            completion(.success(object))
        } catch {
            #if DEBUG
            print("=== Parsing Error \(error) ===")
            #endif
            completion(.failure(BooksServiceError.parsing))
        }
    }
}
