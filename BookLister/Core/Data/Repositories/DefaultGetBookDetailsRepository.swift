//
//  DefaultGetBookDetailsRepository.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

final class DefaultGetBookDetailsRepository: GetBookDetailsRepository {
    private let service: CanGetBookDetailsService
    
    init(service: CanGetBookDetailsService) {
        self.service = service
    }
    
    func getBookDetails(bookID: Int, completion: @escaping (Result<BookDetails, Error>) -> Void) {
        service.getBookDetails(bookID: bookID) { result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let dto):
                    completion(.success(dto.toDomain()))
                }
            }
        }
    }
}
