//
//  DefaultGetBooksRepository.swift
//  BookLister
//
//  Created by Maximus on 28/03/2022.
//

import Foundation

final class DefaultGetBooksRepository: GetBooksRepository {
    private let service: CanGetBooksService
    private let storage: BooksStorage
    
    init(service: CanGetBooksService, storage: BooksStorage) {
        self.service = service
        self.storage = storage
    }
    
    func getBooks(cached: @escaping ([Book]) -> Void, completion: @escaping (Result<[Book], Error>) -> Void) {
        storage.fetchBooks { result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    #if DEBUG
                    print(error)
                    #endif
                case .success(let books):
                    cached(books)
                }
            }
        }
        
        service.getBooks { result in
            DispatchQueue.main.async { [weak self] in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let respoonse):
                    let books = respoonse.map { $0.toDomain() }.sorted(by: { $0.title < $1.title })
                    self?.saveBooks(books)
                    completion(.success(books))
                }
            }
        }
    }
}

// MARK: - Private

private extension DefaultGetBooksRepository {
    func saveBooks(_ books: [Book]) {
        storage.saveBooks(books) { error in
            if let error = error {
                #if DEBUG
                print(error)
                #endif
            }
        }
    }
}
