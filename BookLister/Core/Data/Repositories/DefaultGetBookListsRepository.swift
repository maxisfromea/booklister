//
//  DefaultGetBookListsRepository.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

final class DefaultGetBookListsRepository: GetBookListsRepository {
    private let service: CanGetListsService
    private let storage: BookListsStorage
    
    init(service: CanGetListsService, storage: BookListsStorage) {
        self.service = service
        self.storage = storage
    }
    
    func getBookLists(cached: @escaping ([BookList]) -> Void, completion: @escaping (Result<[BookList], Error>) -> Void) {
        storage.fetchBookLists { result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    #if DEBUG
                    print(error)
                    #endif
                case .success(let bookLists):
                    cached(bookLists)
                }
            }
        }
        
        service.getLists { result in
            DispatchQueue.main.async { [weak self] in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let dto):
                    let bookLists = dto.map { $0.toDomain() }.sorted(by: { $0.title < $1.title })
                    self?.saveBookLists(bookLists)
                    completion(.success(bookLists))
                }
            }
        }
    }
}

// MARK: - Private

private extension DefaultGetBookListsRepository {
    func saveBookLists(_ bookLists: [BookList]) {
        storage.saveBookLists(bookLists) { error in
            if let error = error {
                #if DEBUG
                print(error)
                #endif
            }
        }
    }
}
