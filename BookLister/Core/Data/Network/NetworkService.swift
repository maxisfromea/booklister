//
//  NetworkService.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

protocol NetworkService {
    func execute(_ request: Request, completion: @escaping (Data?, Error?) -> Void)
}

final class DefaultNetworkService: NetworkService {
    private let baseURL: URL
    private let sessionService: URLSession
    
    init(urlString: String, sessionService: URLSession = .shared) {
        guard let url = URL(string: urlString) else { fatalError() }
        
        self.baseURL = url
        self.sessionService = sessionService
    }
    
    func execute(_ request: Request, completion: @escaping (Data?, Error?) -> Void) {
        let url = baseURL.appendingPathComponent(request.path)
        sessionService
            .dataTask(with: url) { data, _, error in
                completion(data, error)
            }
            .resume()
    }
}
