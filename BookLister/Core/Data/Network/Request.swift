//
//  Request.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

enum RequestMethod {
    case get
    
    var stringValue: String {
        switch self {
        case .get: return "GET"
        }
    }
}

protocol Request {
    var path: String { get }
    var method: RequestMethod { get }
    
//    func toURLRequest(baseURL: URL) -> URLRequest
}
//
//extension Request {
//    func toURLRequest(baseURL: URL) -> URLRequest {
//        guard let url = URL(string: path, relativeTo: baseURL) else {
//            fatalError()
//        }
//
//        return URLRequest(url: url)
//    }
//}

struct BookListsRequest: Request {
    let method: RequestMethod = .get
    let path = "/lists"
}

struct BooksRequest: Request {
    let method: RequestMethod = .get
    let path = "/books"
}

struct BookDetailsRequest: Request {
    let method: RequestMethod = .get
    let path: String
    
    init(bookID: Int) {
        path = "/book/\(bookID)"
    }
}

//"https://my-json-server.typicode.com/KeskoSenukaiDigital/assignment/lists"
