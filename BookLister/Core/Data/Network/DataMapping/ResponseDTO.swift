//
//  ResponseDTO.swift
//  BookLister
//
//  Created by Maximus on 26/03/2022.
//

import Foundation

struct BookListResponseDTO: Decodable {
    let id: Int
    let title: String
    
    func toDomain() -> BookList {
        BookList(id: id, title: title)
    }
}

struct BookResponseDTO: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case listID = "list_id"
        case title
        case imageURL = "img"
    }
    
    let id: Int
    let listID: Int
    let title: String
    let imageURL: String
    
    func toDomain() -> Book {
        Book(id: id, listID: listID, title: title, imageURL: imageURL)
    }
}

struct BookDetailsResponseDTO: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case listID = "list_id"
        case isbn
        case author
        case title
        case imageURL = "img"
        case description
    }
    
    let id: Int
    let listID: Int
    let isbn: String?
    let author: String
    let title: String
    let imageURL: String
    let description: String
    
    func toDomain() -> BookDetails {
        BookDetails(
            id: id,
            listID: listID,
            isbn: isbn,
            author: author,
            title: title,
            imageURL: imageURL,
            description: description
        )
    }
}
