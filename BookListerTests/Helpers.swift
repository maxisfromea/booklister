//
//  Helpers.swift
//  BookListerTests
//
//  Created by Maximus on 30/03/2022.
//

import Foundation
@testable import BookLister

// MARK: - Encodable

extension BookListResponseDTO: Encodable {
    enum CodingKeys: CodingKey {
        case id, title
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(title, forKey: .title)
    }
}

extension BookResponseDTO: Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(listID, forKey: .listID)
        try container.encode(title, forKey: .title)
        try container.encode(imageURL, forKey: .imageURL)
    }
}

extension BookDetailsResponseDTO: Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(listID, forKey: .listID)
        try container.encode(isbn, forKey: .isbn)
        try container.encode(author, forKey: .author)
        try container.encode(title, forKey: .title)
        try container.encode(imageURL, forKey: .imageURL)
        try container.encode(description, forKey: .description)
    }
}

extension BookList: Encodable {
    public func encode(to encoder: Encoder) throws {
        enum CodingKeys: CodingKey {
            case id, title
        }
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(title, forKey: .title)
    }
}

extension Book: Encodable {
    enum CodingKeys: CodingKey {
        case id, listID, title, imageURL
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(listID, forKey: .listID)
        try container.encode(title, forKey: .title)
        try container.encode(imageURL, forKey: .imageURL)
    }
}

// MARK: - Equatable

extension BooksServiceError: Equatable {
    public static func == (lhs: BooksServiceError, rhs: BooksServiceError) -> Bool {
        switch (lhs, rhs) {
        case (.noData, .noData):
            return true
        case (.parsing, .parsing):
            return true
        case (.custom(let lhsError), .custom(let rhsError)):
            return lhsError.localizedDescription == rhsError.localizedDescription
        default:
            return false
        }
    }
}
