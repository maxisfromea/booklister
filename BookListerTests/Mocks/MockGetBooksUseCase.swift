//
//  MockGetBooksUseCase.swift
//  BookListerTests
//
//  Created by Maximus on 01/04/2022.
//

import Foundation
@testable import BookLister

final class MockGetBooksUseCase: GetBooksUseCase {
    private(set) var numberOfCalls = 0
    
    var result: Result<[Book], Error>?
    
    func execute(cached: @escaping ([Book]) -> Void, completion: @escaping (Result<[Book], Error>) -> Void) {
        numberOfCalls += 1
        
        if let result = result {
            completion(result)
        }
    }
}
