//
//  MockGetBookListsUseCase.swift
//  BookListerTests
//
//  Created by Maximus on 01/04/2022.
//

import Foundation
@testable import BookLister

final class MockGetBookListsUseCase: GetBookListsUseCase {
    private(set) var numberOfCalls = 0
    
    var result: Result<[BookList], Error>?
    
    func execute(cached: @escaping ([BookList]) -> Void, completion: @escaping (Result<[BookList], Error>) -> Void) {
        numberOfCalls += 1
        
        if let result = result {
            completion(result)
        }
    }
}
