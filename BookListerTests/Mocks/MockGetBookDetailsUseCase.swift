//
//  MockGetBookDetailsUseCase.swift
//  BookListerTests
//
//  Created by Maximus on 01/04/2022.
//

import Foundation
@testable import BookLister

final class MockGetBookDetailsUseCase: GetBookDetailsUseCase {
    private(set) var numberOfCalls = 0
    
    var result: Result<BookDetails, Error>?
    
    func execute(bookID: Int, completion: @escaping (Result<BookDetails, Error>) -> Void) {
        numberOfCalls += 1
        
        if let result = result {
            completion(result)
        }
    }
}
