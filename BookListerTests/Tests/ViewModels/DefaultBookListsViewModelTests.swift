//
//  DefaultBookListsViewModelTests.swift
//  BookListerTests
//
//  Created by Maximus on 01/04/2022.
//

import XCTest
@testable import BookLister

class DefaultBookListsViewModelTests: XCTestCase {
    private var getBookListsUseCase: MockGetBookListsUseCase!
    private var getBooksUseCase: MockGetBooksUseCase!
    private var getBookDetailsUseCase: MockGetBookDetailsUseCase!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_onEmptyBookLists_numberOfSections_isZero() async {
        let sut = makeSUT()
        sut.onReload = {
            XCTAssertEqual(sut.numberOfSections, 0)
        }
        sut.viewDidLoad()
    }
    
    func test_onNonEmptyBookLists_numberOfSections_returnsExpectedNumber() async {
        let lists = [BookList(id: 0, title: "Foo"), BookList(id: 1, title: "Bar")]
        let sut = makeSUT(bookLists: .success(lists))
        sut.onReload = {
            XCTAssertEqual(sut.numberOfSections, 2)
        }
        sut.viewDidLoad()
    }
    
    func test_onInvalidSectionNumber_numberOfRows_returnsNumberOf0() async {
        XCTAssertEqual(makeSUT().numberOfRows(in: 1), 0)
    }
    
    func test_onLessThan5BooksInList_numberOfRows_returnsExpectedNumber() async {
        let lists = [BookList(id: 0, title: "Foo")]
        let books = [
            Book(id: 0, listID: 0, title: "1", imageURL: "1"),
            Book(id: 1, listID: 0, title: "1", imageURL: "1"),
            Book(id: 2, listID: 0, title: "1", imageURL: "1")
        ]
        let sut = makeSUT(bookLists: .success(lists), books: .success(books))
        sut.onReload = {
            XCTAssertEqual(sut.numberOfRows(in: 0), 3)
        }
        sut.viewDidLoad()
    }
    
    func test_onMoreThan5BooksInList_numberOfRows_returnsNumberOf5() async {
        let lists = [BookList(id: 0, title: "Foo")]
        let books = [
            Book(id: 0, listID: 0, title: "1", imageURL: "1"),
            Book(id: 1, listID: 0, title: "1", imageURL: "1"),
            Book(id: 2, listID: 0, title: "1", imageURL: "1"),
            Book(id: 3, listID: 0, title: "1", imageURL: "1"),
            Book(id: 4, listID: 0, title: "1", imageURL: "1"),
            Book(id: 5, listID: 0, title: "1", imageURL: "1")
        ]
        let sut = makeSUT(bookLists: .success(lists), books: .success(books))
        sut.onReload = {
            XCTAssertEqual(sut.numberOfRows(in: 0), 5)
        }
        sut.viewDidLoad()
    }
    
    func test_onEmptyBooks_cellModel_returnsNil() {
        let lists = [BookList(id: 0, title: "Foo")]
        let sut = makeSUT(bookLists: .success(lists))
        sut.onReload = {
            XCTAssertNil(sut.cellModel(at: IndexPath(row: 0, section: 0)))
        }
        sut.viewDidLoad()
    }
    
    func test_onBooksInBookList_cellModel_returnsExpectedModel() {
        let lists = [BookList(id: 0, title: "Foo"), BookList(id: 1, title: "Bar")]
        let books = [
            Book(id: 0, listID: 0, title: "Title", imageURL: "ImageURL"),
            Book(id: 1, listID: 1, title: "Subtitle", imageURL: "Image")
        ]
        let sut = makeSUT(bookLists: .success(lists), books: .success(books))
        sut.viewDidLoad()
        
        let model0 = sut.cellModel(at: IndexPath(row: 0, section: 0))
        let model1 = sut.cellModel(at: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(model0?.title, "Title")
        XCTAssertEqual(model0?.id, 0)
        
        XCTAssertEqual(model1?.title, "Subtitle")
        XCTAssertEqual(model1?.id, 1)
    }
    
    func test_onIncorrectIndexPath_cellModel_returnsNil() {
        let sut = makeSUT()
        sut.viewDidLoad()
        
        XCTAssertNil(sut.cellModel(at: IndexPath(row: 1, section: 1)))
    }
    
    func test_onViewDidLoad_getGetBookListsUseCase_isCalled() {
        makeSUT().viewDidLoad()
        
        XCTAssertEqual(getBookListsUseCase.numberOfCalls, 1)
    }
    
    func test_onViewDidLoad_getBooksUseCase_isCalled() {
        makeSUT().viewDidLoad()
        
        XCTAssertEqual(getBooksUseCase.numberOfCalls, 1)
    }
    
    func test_onDidPullToRefresh_getBookListsUseCase_isCalled() {
        makeSUT().didPullToRefresh()
        
        XCTAssertEqual(getBookListsUseCase.numberOfCalls, 1)
    }
    
    func test_onDidPullToRefresh_getBooksUseCase_isCalled() {
        makeSUT().didPullToRefresh()
        
        XCTAssertEqual(getBooksUseCase.numberOfCalls, 1)
    }
    
    func test_onDidTapAll_actionOnShowBooks_hasExpectedParams() async {
        let bookLists = [
            BookList(id: 0, title: "1"),
            BookList(id: 1, title: "2"),
            BookList(id: 2, title: "3")
        ]
        let books = [
            Book(id: 0, listID: 0, title: "Book0", imageURL: ""),
            Book(id: 1, listID: 1, title: "Book1", imageURL: ""),
            Book(id: 2, listID: 1, title: "Book2", imageURL: "")
        ]
        let actions = BookListsViewModelActions(
            onShowBooks: { selectedBooks, title in
                XCTAssertEqual(selectedBooks[0].title, books[1].title)
                XCTAssertEqual(selectedBooks[1].title, books[2].title)
                XCTAssertEqual(title, bookLists[1].title)
            },
            onShowBookDetails: { _ in
                XCTFail()
            }
        )
        let sut = makeSUT(
            bookLists: .success(bookLists),
            books: .success(books),
            actions: actions
        )
        sut.viewDidLoad()
        
        sut.didTapAll(at: 1)
    }
    
    func test_onDidSelectItem_ifIndexPathValid_getBookDetailsIsCalled() {
        let bookLists = [BookList(id: 0, title: "1")]
        let books = [Book(id: 0, listID: 0, title: "1", imageURL: "")]
        let sut = makeSUT(
            bookLists: .success(bookLists),
            books: .success(books)
        )
        sut.viewDidLoad()
        
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(getBookDetailsUseCase.numberOfCalls, 1)
    }
    
    func test_onDidSelectItem_ifIndexPathInvalid_getBookDetailsIsNotCalled() {
        makeSUT().didSelectItem(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(getBookDetailsUseCase.numberOfCalls, 0)
    }
    
    func test_onDidSelectItem_actionOnShowBookDetails_hasCorrectParams() async {
        let bookList = [BookList(id: 0, title: "1")]
        let books = [Book(id: 0, listID: 0, title: "1", imageURL: "")]
        let bookDetails = BookDetails(
            id: 0,
            listID: 0,
            isbn: "123",
            author: "Foo",
            title: "Bar",
            imageURL: "",
            description: "Descriptions"
        )
        let actions = BookListsViewModelActions(
            onShowBooks: { _, _ in },
            onShowBookDetails: { details in
                XCTAssertEqual(details.id, bookDetails.id)
                XCTAssertEqual(details.isbn, bookDetails.isbn)
                XCTAssertEqual(details.author, bookDetails.author)
                XCTAssertEqual(details.title, bookDetails.title)
                XCTAssertEqual(details.description, bookDetails.description)
            }
        )
        let sut = makeSUT(
            bookLists: .success(bookList),
            books: .success(books),
            bookDetails: .success(bookDetails),
            error: nil,
            actions: actions
        )
        sut.viewDidLoad()
        
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
    }
}

private extension DefaultBookListsViewModelTests {
    func makeSUT(
        bookLists: Result<[BookList], Error> = .success([]),
        books: Result<[Book], Error> = .success([]),
        bookDetails: Result<BookDetails, Error> = .success(BookDetails()),
        error: Error? = nil,
        actions:  BookListsViewModelActions? = nil
    ) -> DefaultBookListsViewModel {
        let actions = actions ?? BookListsViewModelActions(
            onShowBooks: { books, listTitle in
                
            },
            onShowBookDetails: { bookDetails in
                
            }
        )
            
        getBookListsUseCase = MockGetBookListsUseCase()
        getBookListsUseCase.result = bookLists
        
        getBooksUseCase = MockGetBooksUseCase()
        getBooksUseCase.result = books
        
        getBookDetailsUseCase = MockGetBookDetailsUseCase()
        getBookDetailsUseCase.result = bookDetails
        
        return DefaultBookListsViewModel(
            useCases: BookListsViewModelUseCases(
                getBookLists: getBookListsUseCase,
                getBooks: getBooksUseCase,
                getBookDetails: getBookDetailsUseCase
            ),
            actions: actions
        )
    }
}

private extension BookDetails {
    init() {
        self.init(id: 0, listID: 0, isbn: nil, author: "", title: "", imageURL: "", description: "")
    }
}
