//
//  DefaultGetBookListsRepositoryTests.swift
//  BookListerTests
//
//  Created by Maximus on 30/03/2022.
//

import XCTest
@testable import BookLister

class DefaultGetBookListsRepositoryTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_getBookLists_returnsValidResponse() async throws  {
        let bookLists = [
            BookListResponseDTO(id: 0, title: "Bar"),
            BookListResponseDTO(id: 1, title: "Foo")
        ]
        let data = try JSONEncoder().encode(bookLists)
        let sut = DefaultGetBookListsRepository(
            service: DefaultBooksService(
                networkService: FakeNetworkService(
                    data: data,
                    error: nil
                )
            ),
            storage: FakeBookListsStorage()
        )
        
        sut.getBookLists(
            cached: { _ in },
            completion: { result in
                switch result {
                case .success(let lists):
                    XCTAssertEqual(lists.first?.id, 0)
                    XCTAssertEqual(lists.first?.title, "Bar")
                    XCTAssertEqual(lists.last?.id, 1)
                    XCTAssertEqual(lists.last?.title, "Foo")
                default:
                    XCTFail()
                }
            }
        )
    }
    
    func test_getBookLists_onError_returnsError() async {
        let dummyError = DummyError.dummy
        let sut = DefaultGetBookListsRepository(
            service: DefaultBooksService(
                networkService: FakeNetworkService(
                    data: nil,
                    error: dummyError
                )
            ),
            storage: FakeBookListsStorage()
        )
        
        sut.getBookLists(
            cached: { _ in },
            completion: { result in
                switch result {
                case .failure(let error):
                    XCTAssertEqual((error as? BooksServiceError), BooksServiceError.custom(dummyError))
                default:
                    XCTFail()
                }
            }
        )
    }
}
