//
//  DefaultGetBookDetailsRepositoryTests.swift
//  BookListerTests
//
//  Created by Maximus on 30/03/2022.
//

import XCTest
@testable import BookLister

class DefaultGetBookDetailsRepositoryTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_getBookDetails_returnsValidResponse() async throws  {
        let bookDetails = BookDetailsResponseDTO(
            id: 0,
            listID: 1,
            isbn: nil,
            author: "A",
            title: "B",
            imageURL: "C",
            description: "D"
        )
        let data = try JSONEncoder().encode(bookDetails)
        let sut = DefaultGetBookDetailsRepository(
            service: DefaultBooksService(
                networkService: FakeNetworkService(
                    data: data,
                    error: nil
                )
            )
        )
        
        sut.getBookDetails(bookID: 1) { result in
            switch result {
            case .success(let details):
                XCTAssertEqual(details.id, 0)
                XCTAssertEqual(details.listID, 1)
                XCTAssertNil(details.isbn)
                XCTAssertEqual(details.author, "A")
                XCTAssertEqual(details.title, "B")
                XCTAssertEqual(details.imageURL, "C")
                XCTAssertEqual(details.description, "D")
            default:
                XCTFail()
            }
        }
    }
    
    func test_getBookDetails_onError_returnsError() async {
        let dummyError = DummyError.dummy
        let sut = DefaultGetBookDetailsRepository(
            service: DefaultBooksService(
                networkService: FakeNetworkService(
                    data: nil,
                    error: dummyError
                )
            )
        )
        
        sut.getBookDetails(bookID: 1) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual((error as? BooksServiceError), BooksServiceError.custom(dummyError))
            default:
                XCTFail()
            }
        }
    }
}
