//
//  ResponseDTOMappingTests.swift
//  BookListerTests
//
//  Created by Maximus on 30/03/2022.
//

import XCTest
@testable import BookLister

class ResponseDTOMappingTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_bookListResponseDTOMapping_mapsToDomainEntity() {
        let sut = BookListResponseDTO(id: 0, title: "Foo").toDomain()
        
        XCTAssertEqual(sut.id, 0)
        XCTAssertEqual(sut.title, "Foo")
    }

    func testBookResponseDTOMapping_mapsToDomainEntity() {
        let sut = BookResponseDTO(id: 0, listID: 1, title: "Foo", imageURL: "Bar").toDomain()
        
        XCTAssertEqual(sut.id, 0)
        XCTAssertEqual(sut.listID, 1)
        XCTAssertEqual(sut.title, "Foo")
        XCTAssertEqual(sut.imageURL, "Bar")
    }
    
    func testBookDetailsResponseDTOMapping_mapsToDomainEntity() {
        let sut = BookDetailsResponseDTO(
            id: 0,
            listID: 1,
            isbn: nil,
            author: "Boo",
            title: "Foo",
            imageURL: "Bar",
            description: "Mar"
        ).toDomain()
        
        XCTAssertEqual(sut.id, 0)
        XCTAssertEqual(sut.listID, 1)
        XCTAssertNil(sut.isbn)
        XCTAssertEqual(sut.author, "Boo")
        XCTAssertEqual(sut.title, "Foo")
        XCTAssertEqual(sut.imageURL, "Bar")
        XCTAssertEqual(sut.description, "Mar")
    }
}
