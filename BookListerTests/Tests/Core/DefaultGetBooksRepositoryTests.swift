//
//  DefaultGetBooksRepositoryTests.swift
//  BookListerTests
//
//  Created by Maximus on 30/03/2022.
//

import XCTest
@testable import BookLister

class DefaultGetBooksRepositoryTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_getBooks_returnsValidResponse() async throws  {
        let books = [
            BookResponseDTO(id: 0, listID: 1, title: "Foo", imageURL: "Boo"),
            BookResponseDTO(id: 2, listID: 3, title: "Far", imageURL: "Bar")
        ]
        let data = try JSONEncoder().encode(books)
        let sut = DefaultGetBooksRepository(
            service: DefaultBooksService(
                networkService: FakeNetworkService(
                    data: data,
                    error: nil
                )
            ),
            storage: FakeBooksStorage()
        )
        
        sut.getBooks(
            cached: { _ in },
            completion: { result in
                switch result {
                case .success(let lists):
                    XCTAssertEqual(lists.first?.id, 2)
                    XCTAssertEqual(lists.first?.title, "Far")
                    XCTAssertEqual(lists.last?.id, 0)
                    XCTAssertEqual(lists.last?.title, "Foo")
                default:
                    XCTFail()
                }
            }
        )
    }
    
    func test_getBooks_onError_returnsError() async {
        let dummyError = DummyError.dummy
        let sut = DefaultGetBooksRepository(
            service: DefaultBooksService(
                networkService: FakeNetworkService(
                    data: nil,
                    error: dummyError
                )
            ),
            storage: FakeBooksStorage()
        )
        
        sut.getBooks(
            cached: { _ in },
            completion: { result in
                switch result {
                case .failure(let error):
                    XCTAssertEqual((error as? BooksServiceError), BooksServiceError.custom(dummyError))
                default:
                    XCTFail()
                }
            }
        )
    }
}
