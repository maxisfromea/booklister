//
//  BookServiceTests.swift
//  BookListerTests
//
//  Created by Maximus on 28/03/2022.
//

import XCTest
@testable import BookLister

class BookServiceTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_getListsRequest_returnsValidResponse() async {
        let bookListResponse = BookListResponseDTO(id: 0, title: "Foo")
        let data = makeData(from: [bookListResponse])
        let sut = makeSUT(data: data, error: nil)
        
        sut.getLists { result in
            switch result {
            case .success(let response):
                XCTAssertEqual(response.first?.id, bookListResponse.id)
            default:
                XCTFail()
            }
        }
    }
    
    func test_getBooks_returnsValidResponse() async {
        let booksResponse = [
            BookResponseDTO(id: 0, listID: 0, title: "Foo", imageURL: "Foo"),
            BookResponseDTO(id: 1, listID: 1, title: "Bar", imageURL: "Bar")
        ]
        let data = makeData(from: booksResponse)
        let sut = makeSUT(data: data, error: nil)
        
        sut.getBooks { result in
            switch result {
            case .success(let books):
                XCTAssertEqual(books.first?.id, booksResponse.first?.id)
                XCTAssertEqual(books.last?.id, booksResponse.last?.id)
            case .failure:
                XCTFail()
            }
        }
    }
    
    func test_getBookDetails_returnsValidResponse() async {
        let bookDetailsResponse = BookDetailsResponseDTO(
            id: 0,
            listID: 0,
            isbn: nil,
            author: "Foo",
            title: "Foo",
            imageURL: "Foo",
            description: "Foo"
        )
        let data = makeData(from: bookDetailsResponse)
        let sut = makeSUT(data: data, error: nil)
        
        sut.getBookDetails(bookID: 0) { result in
            switch result {
            case .success(let details):
                XCTAssertEqual(details.id, bookDetailsResponse.id)
            default:
                XCTFail()
            }
        }
    }
    
    func test_bookSservice_returnsNoDataError_ifDataIsNil() async {
        let sut = makeSUT(data: nil, error: nil)
        
        sut.getLists { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, BooksServiceError.noData)
            default:
                XCTFail()
            }
        }
    }
    
    func test_booksService_returnsParsingError_ifCannotParseResponse() async {
        let sut = makeSUT(data: Data(), error: nil)
        
        sut.getBooks { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, BooksServiceError.parsing)
            default:
                XCTFail()
            }
        }
        sut.getBookDetails(bookID: 0) { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, BooksServiceError.parsing)
            default:
                XCTFail()
            }
        }
        sut.getLists { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, BooksServiceError.parsing)
            default:
                XCTFail()
            }
        }
    }
    
    func test_booksService_returnsCustomError_ifNetworkServiceReturnsError() async {
        let sut = makeSUT(data: nil, error: BooksServiceError.noData)
        
        sut.getLists { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error, BooksServiceError.custom(BooksServiceError.noData))
            default:
                XCTFail()
            }
        }
    }
}

// MARK: - Private

private extension BookServiceTests {
    func makeSUT(data: Data?, error: Error?) -> DefaultBooksService {
        DefaultBooksService(networkService: FakeNetworkService(data: data, error: error))
    }
    
    func makeData<T: Encodable>(from encodable: T, file: String = #file, line: UInt = #line) -> Data {
        guard let data = try? JSONEncoder().encode(encodable) else {
            fatalError()
        }
        
        return data
    }
}
