//
//  DummyError.swift
//  BookListerTests
//
//  Created by Maximus on 30/03/2022.
//

import Foundation

enum DummyError: Error {
    case dummy
    
    var localizedDescription: String {
        "Dummy"
    }
}
