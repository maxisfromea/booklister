//
//  FakePersistentStorage.swift
//  BookListerTests
//
//  Created by Maximus on 07/04/2022.
//

import Foundation
@testable import BookLister

final class FakeBookListsStorage: BookListsStorage {
    func saveBookLists(_ bookLists: [BookList], completion: @escaping (Error?) -> Void) { }
    func fetchBookLists(completion: @escaping (Result<[BookList], Error>) -> Void) { }
}

final class FakeBooksStorage: BooksStorage {
    func saveBooks(_ books: [Book], completion: @escaping (Error?) -> Void) { }
    func fetchBooks(completion: @escaping (Result<[Book], Error>) -> Void) { }
}
