//
//  FakeNetworkService.swift
//  BookListerTests
//
//  Created by Maximus on 28/03/2022.
//

import Foundation
@testable import BookLister

final class FakeNetworkService: NetworkService {
    var data: Data?
    var error: Error?
    
    init(data: Data?, error: Error?) {
        self.data = data
        self.error = error
    }
    
    func execute(_ request: Request, completion: @escaping (Data?, Error?) -> Void) {
        completion(data, error)
    }
}
